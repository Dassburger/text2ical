from unittest import TestCase

from text2ical import CalendarDate, Event, Schedule
from text2ical_ui import encode_date, decode_date, encode_event, decode_event, encode_schedule, \
    decode_schedule


class TestUi(TestCase):
    def test_encode_date(self):
        self.assertDictEqual({"day": 13, "month": 5, "year": 1985}, encode_date(CalendarDate(5, 13, year=1985)))
        self.assertDictEqual({"day": 13, "month": 5, "year": None}, encode_date(CalendarDate(5, 13)))

    def test_decode_date(self):
        self.assertEqual(CalendarDate(5, 13, year=1985), decode_date({"day": 13, "month": 5, "year": 1985}))
        self.assertEqual(CalendarDate(5, 13), decode_date({"day": 13, "month": 5, "year": None}))

    def test_encode_event(self):
        self.assertDictEqual({
            "summary": "summary",
            "start": {"day": 13, "month": 2, "year": 2018},
            "end": None,
            "description": "desc",
        }, encode_event(Event("summary", CalendarDate(2, 13, 2018), description="desc")))
        self.assertDictEqual({
            "summary": "summary",
            "start": {"day": 13, "month": 2, "year": 2018},
            "end": {"day": 15, "month": 2, "year": 2018},
            "description": "desc",
        }, encode_event(Event("summary", CalendarDate(2, 13, 2018), CalendarDate(2, 15, 2018), description="desc")))

    def test_decode_event(self):
        self.assertEqual(Event("summary", CalendarDate(2, 13, 2018), description="desc"),
                         decode_event({
                             "summary": "summary",
                             "start": {"day": 13, "month": 2, "year": 2018},
                             "end": None,
                             "description": "desc",
                         }))
        self.assertEqual(Event("summary", CalendarDate(2, 13, 2018), CalendarDate(2, 15, 2018), description="desc"),
                         decode_event({
                             "summary": "summary",
                             "start": {"day": 13, "month": 2, "year": 2018},
                             "end": {"day": 15, "month": 2, "year": 2018},
                             "description": "desc",
                         }))

    def test_encode_decode_schedule(self):
        schedule = Schedule([
                Event("e1", CalendarDate(2, 1, 2018)),
                Event("e2", CalendarDate(3, 2, 2018))
            ])
        schedule.summary = "summary"
        json = {
                "summary": "summary",
                "events": [
                    {"summary": "e1", "start": {"day": 1, "month": 2, "year": 2018}, "end": None, "description": None},
                    {"summary": "e2", "start": {"day": 2, "month": 3, "year": 2018}, "end": None, "description": None},
                ]
            }
        self.assertDictEqual(json, encode_schedule(schedule))
        self.assertEqual(schedule, decode_schedule(json))
