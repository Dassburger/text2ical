from unittest import TestCase

from text2ical import Schedule, CalendarDate, Event

SAMPLE_DATA = """
Here’s the high level schedule of Space Flights

Buran: Feb 23
Appollo 13: Feb 26 – Mar 2
Voyager: Mar 2
XX 1 Curiosity: Mar 7 - 20
XX 1 Endeavor: Mar 9 - 22
XX 2 Curiosity: Mar 23 - 29
XX 2 Endeavor: Mar 27 - 29
Challenger: Mar 30
XX 3 Curiosity starts: Apr 3
XX 3 Endeavor starts: Apr 5
XX AB New: Apr 12
GW AB Old: Apr 19
"""


class TestEvent(TestCase):
    def test_str(self):
        self.assertEqual("'summary': start - end, \"desc\"", str(Event("summary", "start", "end", "desc")))
        self.assertEqual("'summary': start, \"description\"", str(Event("summary", "start", description="description")))
        self.assertEqual("<E 'summary': start - end, \"desc\">", repr(Event("summary", "start", "end", "desc")))
        self.assertEqual("<E 'summary': start, \"desc\">", repr(Event("summary", "start", description="desc")))

    def test_parsing_single_date_event(self):
        event = Event.from_string("Buran: May 9")
        self.assertIsNotNone(event)
        self.assertEqual("Buran", event.summary)
        self.assertEqual("Buran: May 9", event.description)
        self.assertEqual(CalendarDate(5, 9), event.start)
        self.assertIsNone(event.end)

    def test_parsing_date_range_event(self):
        event = Event.from_string("XX 1 Endeavor: May 25 – June 7")
        self.assertIsNotNone(event)
        self.assertEqual("XX 1 Endeavor", event.summary)
        self.assertEqual("XX 1 Endeavor: May 25 – June 7", event.description)
        self.assertEqual(CalendarDate(5, 25), event.start)
        self.assertEqual(CalendarDate(6, 7), event.end)

    def test_parsing_date_range_across_year(self):
        event = Event.from_string("XX 1 Endeavor: Dec 25 – January 7")
        self.assertIsNotNone(event)
        self.assertEqual("XX 1 Endeavor", event.summary)
        self.assertEqual("XX 1 Endeavor: Dec 25 – January 7", event.description)
        self.assertEqual(CalendarDate(12, 25), event.start)
        self.assertEqual(CalendarDate(1, 7), event.end)

    def test_parsing_date_range_same_month_event(self):
        event = Event.from_string("XX 1 Endeavor: May 17 - 25")
        self.assertIsNotNone(event)
        self.assertEqual("XX 1 Endeavor", event.summary)
        self.assertEqual("XX 1 Endeavor: May 17 - 25", event.description)
        self.assertEqual(CalendarDate(5, 17), event.start)
        self.assertEqual(CalendarDate(5, 25), event.end)


class TestCalendarDate(TestCase):
    def test_attributes(self):
        d = CalendarDate(5, 13)
        self.assertEqual(5, d.month)
        self.assertEqual(13, d.day)

    def test_month_validation(self):
        with self.assertRaises(ValueError):
            CalendarDate(0, 13)
        with self.assertRaises(ValueError):
            CalendarDate(-1, 13)
        with self.assertRaises(ValueError):
            CalendarDate(13, 13)

    def test_day_validation(self):
        with self.assertRaises(ValueError):
            CalendarDate(5, 0)
        with self.assertRaises(ValueError):
            CalendarDate(5, -1)
        with self.assertRaises(ValueError):
            CalendarDate(5, 32)

    def test_str(self):
        self.assertEqual("5/13", str(CalendarDate(5, 13)))
        self.assertEqual("5/13 2017", str(CalendarDate(5, 13, year=2017)))
        self.assertEqual("<D 5/13>", repr(CalendarDate(5, 13)))
        self.assertEqual("<D 5/13 2017>", repr(CalendarDate(5, 13, year=2017)))

    def test_equality_comparison(self):
        self.assertEqual(CalendarDate(5, 13), CalendarDate(5, 13))
        self.assertEqual(CalendarDate(5, 13, year=2017), CalendarDate(5, 13))
        self.assertEqual(CalendarDate(5, 13), CalendarDate(5, 13, year=2017))
        self.assertEqual(CalendarDate(5, 13, year=2017), CalendarDate(5, 13, year=2017))
        self.assertNotEqual(CalendarDate(5, 13), CalendarDate(5, 14))
        self.assertNotEqual(CalendarDate(5, 13), CalendarDate(4, 13))
        self.assertNotEqual(CalendarDate(5, 13, year=2016), CalendarDate(5, 13, year=2017))

    def test_mutual_comparison(self):
        self.assertLess(CalendarDate(5, 13), CalendarDate(5, 14))
        self.assertLess(CalendarDate(5, 13, year=2017), CalendarDate(5, 14))
        self.assertLess(CalendarDate(5, 13), CalendarDate(5, 14, year=2017))
        self.assertLess(CalendarDate(5, 13, year=2017), CalendarDate(5, 14, year=2018))
        self.assertGreater(CalendarDate(5, 14), CalendarDate(5, 13))
        self.assertGreater(CalendarDate(5, 14, year=2017), CalendarDate(5, 13))
        self.assertGreater(CalendarDate(5, 14), CalendarDate(5, 13, year=2017))
        self.assertGreater(CalendarDate(5, 14, year=2018), CalendarDate(5, 13, year=2017))
        self.assertLess(CalendarDate(5, 13), CalendarDate(6, 3))
        self.assertLess(CalendarDate(5, 13, year=2017), CalendarDate(6, 3))
        self.assertLess(CalendarDate(5, 13), CalendarDate(6, 3, year=2017))
        self.assertLess(CalendarDate(5, 13, year=2017), CalendarDate(6, 3, year=2017))

    def test_parsing(self):
        self.assertEqual(CalendarDate(1, 13), CalendarDate.from_string("January 13"))
        self.assertEqual(CalendarDate(1, 13), CalendarDate.from_string("Jan 13"))

        self.assertEqual(CalendarDate(2, 13), CalendarDate.from_string("February 13"))
        self.assertEqual(CalendarDate(2, 13), CalendarDate.from_string("Feb 13"))

        self.assertEqual(CalendarDate(3, 13), CalendarDate.from_string("March 13"))
        self.assertEqual(CalendarDate(3, 13), CalendarDate.from_string("Mar 13"))

        self.assertEqual(CalendarDate(4, 13), CalendarDate.from_string("April 13"))
        self.assertEqual(CalendarDate(4, 13), CalendarDate.from_string("Apr 13"))

        self.assertEqual(CalendarDate(5, 13), CalendarDate.from_string("May 13"))

        self.assertEqual(CalendarDate(6, 13), CalendarDate.from_string("June 13"))
        self.assertEqual(CalendarDate(6, 13), CalendarDate.from_string("Jun 13"))

        self.assertEqual(CalendarDate(7, 13), CalendarDate.from_string("July 13"))
        self.assertEqual(CalendarDate(7, 13), CalendarDate.from_string("Jul 13"))

        self.assertEqual(CalendarDate(8, 13), CalendarDate.from_string("August 13"))
        self.assertEqual(CalendarDate(8, 13), CalendarDate.from_string("Aug 13"))

        self.assertEqual(CalendarDate(9, 13), CalendarDate.from_string("September 13"))
        self.assertEqual(CalendarDate(9, 13), CalendarDate.from_string("Sep 13"))

        self.assertEqual(CalendarDate(10, 13), CalendarDate.from_string("October 13"))
        self.assertEqual(CalendarDate(10, 13), CalendarDate.from_string("Oct 13"))

        self.assertEqual(CalendarDate(11, 13), CalendarDate.from_string("November 13"))
        self.assertEqual(CalendarDate(11, 13), CalendarDate.from_string("Nov 13"))

        self.assertEqual(CalendarDate(12, 13), CalendarDate.from_string("December 13"))
        self.assertEqual(CalendarDate(12, 13), CalendarDate.from_string("Dec 13"))

    def test_parsing_unparseable(self):
        with self.assertRaises(ValueError):
            CalendarDate.from_string("May 13 1985")
        with self.assertRaises(ValueError):
            CalendarDate.from_string("May13")
        with self.assertRaises(ValueError):
            CalendarDate.from_string("May")
        with self.assertRaises(ValueError):
            CalendarDate.from_string("13")
        with self.assertRaises(ValueError):
            CalendarDate.from_string("complete garbage")
        with self.assertRaises(ValueError):
            CalendarDate.from_string("")


class TestSchedule(TestCase):
    def test_schedule_attributes(self):
        s = Schedule([1, 2, 3], summary="summary")
        self.assertListEqual([1, 2, 3], s.events)
        self.assertEqual("summary", s.summary)

    def test_inflating_dates_assigns_and_overwrites_year_of_events(self):
        def event_dates_years(ev):
            if ev.end is not None:
                return ev.start.year, ev.end.year
            else:
                return ev.start.year

        schedule = Schedule([
            Event("A", CalendarDate(12, 3, 2022)),
            Event("B", CalendarDate(12, 22, 2015), CalendarDate(1, 31, 2017)),
            Event("B1", CalendarDate(5, 13), CalendarDate(5, 17)),
            Event("C", CalendarDate(6, 23, 2018)),
            Event("D", CalendarDate(1, 23)),
        ]).inflate_dates(2018)
        self.assertListEqual([2016, (2016, 2017), (2017, 2017), 2017, 2018],
                             [event_dates_years(ev) for ev in schedule.events])

    def test_reading_text_and_generating_ical(self):
        ical = Schedule.from_strings(SAMPLE_DATA.splitlines()).inflate_dates(2018).to_ical()
        self.assertTrue(ical)
        self.assertIsInstance(ical, str)
        self.assertTrue("VCALENDAR" in ical)
