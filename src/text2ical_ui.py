"""
UI for the release schedule converter.

Following functions are exposed through eel to be used in the UI:

* `text2events` - converts email text into the list of events for a given release year;
* `import_events` - opens with the OS default application an iCalendar with the release events;
* `events2ical` - converts the list of events in iCalendar format.
"""

import eel

import text2ical


def encode_date(d):
    """
    Encode a `CalendarDate` object into dictionary which can be serialized to JSON by eel.

    The dictionary has following attributes:

    * `month`;
    * `day`;
    * `year` - year on `None`.

    :param d: `CalendarDate` to encode
    :return: dictionary for conversion to JSON by eel
    """
    return {"month": d.month, "day": d.day, "year": d.year}


def decode_date(d):
    """
    Decode a dictionary with month, day, and year into `CalendarDate`.

    :param d: dictionary to decode
    :return: `CalendarDate` instance
    """
    return text2ical.CalendarDate(**d)


def encode_event(event):
    """
    Encode an `Event` into a dictionary.

    The dictionary contains the same keys as the attributes of the `Event`.

    :param event: `Event` to encode
    :return: a dictionary
    """
    return {
        "summary": event.summary,
        "start": encode_date(event.start),
        "end": encode_date(event.end) if event.end is not None else None,
        "description": event.description,
    }


def decode_event(event):
    """
    Decode a dictionary into `Event` object.

    :param event: dictionary
    :return: `Event` instance
    """
    event = text2ical.Event(**event)
    event.start = decode_date(event.start)
    if event.end is not None:
        event.end = decode_date(event.end)
    return event


def encode_schedule(schedule):
    """
    Encode a schedule into dictionary.

    A dictionary has the following form::
        {
            "summary": "Schedule summary",
            "events": [{event dictionary}, {event dictionary}, ...]
        }

    :param schedule: a `Schedule` instance to encode
    :return: a dictionary that can be serialized to JSON
    """
    return {
        "summary": schedule.summary,
        "events": [encode_event(e) for e in schedule.events]
    }


def decode_schedule(schedule):
    """
    Decodes a dictionary into a `Schedule` instance.

    A dictionary has the following form::
        {
            "summary": "Schedule summary",
            "events": [{event dictionary}, {event dictionary}, ...]
        }

    :param schedule: dictionary of schedule
    :return: a `Schedule` instance
    """
    return text2ical.Schedule([decode_event(e) for e in schedule["events"]], schedule["summary"])


@eel.expose
def text2schedule(release_year, summary, email_text):
    """
    Convert email text to a schedule (as JSON).

    :param release_year: release year
    :param summary: schedule summary
    :param email_text: email
    :return: dictionary for the schedule in the `email_text`
    """
    schedule = text2ical.Schedule.from_strings(email_text.splitlines()).inflate_dates(int(release_year))
    schedule.summary = summary
    return encode_schedule(schedule)


@eel.expose
def import_schedule(schedule):
    """
    Converts `schedule` JSON dictionary into iCalendar, and opens it with the OS default application.

    :param schedule: JSON dictionary for a `Schedule` (see `encode_schedule()`/`decode_schedule()`)
    """
    text2ical.import_calendar(decode_schedule(schedule).to_ical())


@eel.expose
def schedule2ical(schedule):
    """
    Converts schedule JSON dictionary into iCalendar

    :param schedule: `Schedule` JSON dictionary (see `encode_events()`)
    :return: iCalendar as a string
    """
    return decode_schedule(schedule).to_ical()


if __name__ == "__main__":
    eel.init("src/ui")
    eel.start("index.html")
