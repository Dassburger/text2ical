#!/usr/bin/python3

"""
A tool to convert a release manager email with the release schedule in iCalendar.

Most important notions used here:

* release schedule - a list of major events/milestones of a release
* release year - the year a release goes public, it's the year of the last event in the schedule
* event/phase/milestone - an item in the release schedule, it can be a single day event, or be a phase which lasts for
  several days and has a `start` date, and `end` date.
"""

import argparse
import os
import re
import subprocess
import sys
import tempfile
import uuid
from datetime import date, timedelta, datetime

from icalendar import Calendar, Event as IcalEvent


class Event:
    """
    An event from the release schedule. Can be either a one day event, or a phase with a start and end dates.

    An event has following attributes:

    * `summary` - short summary of an event;
    * `start` - day of the event, or the first day of a phase;
    * `end` - last day of a phase, or `None`;
    * `description` - more verbose description of the event or phase, usually, the line of the email this event is
      constructed from

    If the event is a single day event, `end` attribute is `None`.
    """

    def __init__(self, summary, start, end=None, description=None):
        """
        Constructor of the event

        :param summary: summary
        :param start: date of the event, or first day of the phase
        :param end: last day of the phase, or `None`
        :param description: verbose description of the event or phase
        """
        self.summary = summary
        self.description = description
        self.start = start
        self.end = end

    def __str__(self):
        """
        String representation of the event.

        :return: string representation of the event
        """
        if self.end:
            return "'%(summary)s': %(start)s - %(end)s, \"%(description)s\"" % self.__dict__
        else:
            return "'%(summary)s': %(start)s, \"%(description)s\"" % self.__dict__

    def __repr__(self):
        """
        Technical string representation of the event.

        :return: technical string representation
        """
        return "<E %s>" % (self.__str__(),)

    def __eq__(self, other):
        """
        Compares to events for equality.

        Events are considered equal if all attributes are equal.

        :param other: the `Event` to compare to
        :return: `True` or `False
        """
        return self.summary == other.summary and self.description == other.description and \
               self.start == other.start and self.end == other.end

    @staticmethod
    def from_string(line):
        """
        Create a schedule `Event` from a `line` of text.

        The line of text is like one of the following:

        * 'Event summary: Mar 14', single day event;
        * 'Event summary: Mar 14 - 22', phase within one month;
        * 'Event summary: Mar 14 - Apr 27', phase spanning over several months.
        '
        :param line: line of text
        :return:
        """

        def as_one_day(s):
            event_str = PATTERN_EVT_ONE_DAY.match(s)
            if event_str:
                summary = event_str.group(1)
                start = CalendarDate.from_string(event_str.group(2))
                return Event(summary, start, description=s)

        def as_same_month_period(s):
            event_str = PATTERN_EVT_WITHIN_MONTH.match(s)
            if event_str:
                summary = event_str.group(1)
                start = CalendarDate.from_string(event_str.group(2))
                end = int(event_str.group(3))
                return Event(summary, start, CalendarDate(start.month, end), description=s)

        def as_different_months_period(s):
            event_str = PATTERN_EVT_ACROSS_MONTHS.match(s)
            if event_str:
                summary = event_str.group(1)
                start = CalendarDate.from_string(event_str.group(2))
                end = CalendarDate.from_string(event_str.group(3))
                return Event(summary, start, end, description=s)

        line = line.strip()
        for p in (as_one_day, as_same_month_period, as_different_months_period):
            event = p(line)
            if event:
                return event


PATTERN_EVT_ONE_DAY = re.compile(r"^(.+):\s*(\w+\s+\d+)$")
PATTERN_EVT_WITHIN_MONTH = re.compile(r"^(.+):\s*(\w+\s+\d+)\s+[\S\W]\s+(\d+)$")
PATTERN_EVT_ACROSS_MONTHS = re.compile(r"^(.+):\s*(\w+\s+\d+)\s+[\S\W]\s+(\w+\s+\d+)$")


class Schedule:
    """
    Schedule is a list of events with optional `summary`.
    """

    def __init__(self, events, summary=None):
        """
        Create new schedule as list of events.
        """
        self.events = events
        self.summary = summary

    def __eq__(self, other):
        """
        Check `other` for equality with `self`. Schedules are considered equal if
        all events are equal and in the same order, and summaries are equal too.

        :param other: a `Schedule` to compare with
        :return: `True`/`False`
        """
        return self.summary == other.summary and self.events == other.events

    def inflate_dates(self, release_year):
        """
        Analyze list of events for the given `release_year`, and fill event dates with the year of corresponding
        event. This operation overwrites events dates, even if they already have year assigned.

        The `release_year` is the year the release goes public. E.g., its the year of the last event in the schedule.
        In the simplest, and most common case, the same year is applied to all dates in the schedule. For X.0
        releases, when the development starts in Q4 of the previous year, this function determines year boundaries
        from the dates and fills the dates accordingly.

        :param release_year: year the release goes public
        :return: `self`
        """
        prev_start = CalendarDate(12, 31, year=release_year)
        for ev in reversed(self.events):
            # clear already assigned year
            ev.start.year = None
            if ev.end is not None:
                ev.end.year = None
            # inflate
            if ev.start > prev_start:
                ev.start.year = prev_start.year - 1
            else:
                ev.start.year = prev_start.year
            if ev.end is not None:
                if ev.end < ev.start:
                    ev.end.year = ev.start.year + 1
                else:
                    ev.end.year = ev.start.year
                assert ev.end is None or ev.start <= ev.end
            assert ev.start <= prev_start
            prev_start = ev.start
        return self

    @staticmethod
    def from_strings(lines):
        """
        Read the list of lines of text and convert them to a schedule with the list of events. All event dates are
        filled with year.

        :param lines: lines of release schedule text
        :return: `Schedule` instance with events restored from `lines` of text
        """
        return Schedule(list(filter(None, [Event.from_string(line.strip()) for line in lines])))

    def to_ical(self):
        """
        Converts the schedule to iCalendar format and returns it as a string.

        All event dates must have theirs year filled, otherwise the function throws a `ValueError`.

        :return: iCalendar as a string
        :raise ValueError: if not all event dates have year filled
        """

        def as_ical_event(event):
            def as_date(calendar_date):
                return date(calendar_date.year, calendar_date.month, calendar_date.day)

            ical = IcalEvent()
            ical.add("uid", uuid.uuid4())
            ical.add("dtstamp", datetime.now())
            ical.add("summary", event.summary)
            if event.description is not None:
                ical.add("description", event.description)
            ical.add("dtstart", as_date(event.start))
            if event.end is not None:
                ical.add("dtend", as_date(event.end) + timedelta(days=1))
            return ical

        calendar = Calendar()
        calendar["summary"] = self.summary or "A Schedule"
        calendar["version"] = "2.0"
        calendar["prodid"] = "text2ical 2018-04-08"

        for evt in self.events:
            calendar.add_component(as_ical_event(evt))
        return calendar.to_ical().decode("utf-8")


class CalendarDate:
    """
    A calendar date having a `day` of month, `month` and `year` (or `None`).

    No validations are made on the validity of a date, except the basic ones.
    """

    def __init__(self, month, day, year=None):
        """
        Construct a `CalendarDate`

        :param month: number of month (1-12)
        :param day: day of the month (1-31)
        :param year: year or `None` if unknown yet
        :raise ValueError: if `month` or `day` is out of range
        """
        if not (1 <= month <= 12):
            raise ValueError("month: %d" % (month,))
        if not (1 <= day <= 31):
            raise ValueError("day: %d" % (day,))
        self.month = month
        self.day = day
        self.year = year

    def __str__(self):
        """
        String representation of the date

        :return: string representation of the date
        """
        if self.year is not None:
            return "%(month)d/%(day)d %(year)d" % self.__dict__
        else:
            return "%(month)d/%(day)d" % self.__dict__

    def __repr__(self):
        """
        Technical string representation of the date.

        :return: Technical string representation of the date
        """
        return "<D %s>" % (self.__str__(),)

    def __eq__(self, other):
        """
        Compares to dates for equality.

        :param other: date to compare to
        :return: `True` or `False`
        """
        return self.month == other.month and self.day == other.day and \
               (self.year is None or other.year is None or self.year == other.year)

    def __lt__(self, other):
        """
        Compares this date to be less (earlier) than `other`.

        :param other: date to compare to
        :return: `True` if `self` is strictly less (earlier) than `other`
        """
        return self.year is not None and other.year is not None and self.year < other.year or \
               self.month < other.month or self.month == other.month and self.day < other.day

    def __le__(self, other):
        """
        Compares this date to be less-or-equal (earlier or same) than `other`.

        :param other: date to compare to
        :return: `True` if `self` is less-or-equal (earlier or same) than `other`
        """
        return self == other or self < other

    @staticmethod
    def from_string(string: str):
        """
        Construct a calendar date from a string like 'Mar 14'.

        :param string: string to parse as a `CalendarDate`
        :return: new `CalendarDate` instance
        :raise ValueError: if `string` is unparseable
        """

        def raise_error(value):
            raise ValueError("Unparseable date: %s" % (value,))

        months = {"january": 1, "jan": 1, "february": 2, "feb": 2, "march": 3, "mar": 3, "april": 4, "apr": 4, "may": 5,
                  "june": 6, "jun": 6, "july": 7, "jul": 7, "august": 8, "aug": 8, "september": 9, "sep": 9, "sept": 9,
                  "october": 10, "oct": 10, "november": 11, "nov": 11, "december": 12, "dec": 12}
        parts = string.split()
        if len(parts) != 2:
            raise_error(string)
        try:
            return CalendarDate(months[parts[0].lower()], int(parts[1]))
        except KeyError:
            raise_error(string)


def start_file(file_path):
    """
    Open a file using OS default application for its type.

    :param file_path: file to open
    """
    if sys.platform.startswith('darwin'):
        subprocess.call(('open', file_path))
    elif os.name == 'nt':
        os.startfile(file_path)
    elif os.name == 'posix':
        subprocess.call(('xdg-open', file_path))


def import_calendar(calendar):
    """
    Opens an iCalendar provided as the string argument `calendar` in OS default application for such files. Usually,
    it's Outlook.

    :param calendar: iCalendar as a string
    """
    calendar_file_name = os.path.join(tempfile.mkdtemp(), 'calendar.ics')
    with open(calendar_file_name, 'w') as f:
        f.write(calendar)
    start_file(calendar_file_name)


def main():
    cmd_parser = argparse.ArgumentParser(description="""Create Release calendar from email. By default, converts 
    'infile' to iCalendar format and opens it with OS default associated program. If '-o' is specified, 
    writes iCalendar to specified destination.""")
    cmd_parser.add_argument("infile", type=argparse.FileType("r"), nargs="?", default=sys.stdin,
                            help="Input file (default - stdin) copy-pasted from email.")
    cmd_parser.add_argument("-o", "--output", type=argparse.FileType("w"), nargs="?",
                            help="Output file (default - stdout) in iCalendar format.")
    cmd_parser.add_argument("-y", "--year", type=int, help="Target release year. Default - current year.",
                            default=date.today().year)
    cmd_parser.add_argument("-s", "--summary", type=str, nargs="?", help="Optional calendar summary")
    args = cmd_parser.parse_args()
    schedule = Schedule.from_strings(args.infile.readlines()).inflate_dates(args.year)
    if args.summary is not None:
        schedule.summary = args.summary
    cal = schedule.to_ical()
    if args.output is not None:
        args.output.write(cal)
    else:
        import_calendar(cal)


if __name__ == "__main__":
    main()
