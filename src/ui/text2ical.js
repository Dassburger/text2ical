var schedule = null;

function schedule_preview() {
    var email_text = document.getElementById("email_text").value;
    var release_year = document.getElementById("release_year").value;
    var summary = document.getElementById("schedule_summary").value;
    eel.text2schedule(release_year, summary, email_text)(function (response) {
        var events_ui = document.getElementById("events");
        clear(events_ui);
        schedule = response;
        for (let event of schedule.events)
            events_ui.appendChild(event_preview(event));
        var save_btn = document.getElementById("schedule_save_btn");
        var open_btn = document.getElementById("schedule_open_btn");
        save_btn.disabled = false;
        open_btn.disabled = false;
    });
}
function schedule_enable_preview() {
    var preview_btn = document.getElementById("schedule_preview_btn");
    var email_text = document.getElementById("email_text");
    preview_btn.disabled = email_text.value == "";
}
function schedule_open() {
    if (schedule == null)
        advise_to_paste_email();
    else
        eel.import_schedule(schedule);
}
function schedule_save() {
    if (schedule == null) {
        advise_to_paste_email();
    } else {
        var ical = eel.schedule2ical(schedule)(function (response){
            save_as_file(response);
        });
    }
}




function advise_to_paste_email() {
    alert("Please, copy-paste an email with release schedule and click 'Preview'.");
}
function save_as_file(ical) {
    var a = document.createElement("a");
    a.href = URL.createObjectURL(new Blob([ical], {type: "text/calendar"}));
    a.download = "calendar.ics";
    a.click();
}
function date2text(d) {
    return d.month + "/" + d.day + " " + d.year;
}
function event2text(event) {
    var s = date2text(event.start);
    if (event.end) {
        s += " - " + date2text(event.end);
    }
    s += ": " + event.summary + "\n";
    return s;
}
function event_preview(event) {
    var node = document.createElement("LI");
    var text_node = document.createTextNode(event2text(event));
    node.appendChild(text_node);
    return node;
}
function clear(ui) {
    while (ui.firstChild) {
        ui.removeChild(ui.firstChild);
    }
}
