# Text2ical

Create iCalendar from release managers' email about release schedule.

# Development

## Prepare Environment

Create a virtual env for this project, and install required packages from `requirements.txt`.

## Switching to Virtualenv

Before running any commands, switch to project Virtualenv.:

	$ source ./venv/bin/activate

## Run Tests

Following command runs all unit tests.

	$ python3 -m unittest discover -s test

### With Coverage

Running Unit Tests with Coverage.Py

	$ PYTHONPATH=src coverage run --source=src -m unittest discover -s test && coverage html -d test_coverage

## Source Control Management

This project uses [Fossil](https://www.fossil-scm.org/) as its SCM. Following are the most important commands.

Open a repository from a local file:

	$ mkdir project_working_copy_dir && cd project_working_copy_dir && fossil open path_to/project_repo_file.fossil

Commit changes (and save them to the repository file):

	$ fossil commit

Run Fossil UI in the web browser:

	$ fossil ui

## Build a Binary Executable

Requires `pyinstaller`:

    $ pip install pyinstaller

Having it installed, execute following command from the project root (remove `--noconsole` on Windows):

    $ PYTHONPATH=src python -m eel src/text2ical_ui.py src/ui --onefile --noconsole

This will create an executable file in `dist` directory.

Note. `--noconsole` needs to be removed on Windows because eel is spawning another process, and something gets messed
up with stdin/stdout/stderr handling. More info here: 
https://stackoverflow.com/questions/24455337/pyinstaller-on-windows-with-noconsole-simply-wont-work